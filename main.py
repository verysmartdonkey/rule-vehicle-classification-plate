# author:kiki

from os import sys
sys.path.append("../../")
from vehicle_classification_plate.nodeflux.analytics.vehicle_plate_detection import VehiclePlateDetection
from vehicle_classification_plate.nodeflux.analytics.lpr_detection import *
from post_processing import PostProcessing
from nodeflux.io_utils.file_function import *
import cv2
from nodeflux.image_utils.image_function import *
from  nodeflux.image_utils.color_quantization.restore import *
import os

logger = Logger("LPR")

config_file = expanduser("~")+"/.nodeflux/rules/rule-license-plate-recognition/config.json"
data_cfg = json.load(open(config_file))
temp_dir_path = os.path.join(expanduser("~")+"/.nodeflux","tmp")

class VehicleClassificationPlateMain(PostProcessing):

    def dump_data(self):
        """ Dumping data to server using API.

        Arguments:
            dump_db {Dictionary} -- Dumping data.
            image {image} -- Postprocess license plate image.

        """
        while True:
            time.sleep(0.2)
            for i, dump_db_item in reversed(list(enumerate(self.dump_db))):
                try:

                    thumbnail_image = dump_db_item['plate_image']
                    thumbnail_name = dump_db_item['thumbnail']
                    cv2.imwrite(thumbnail_name, thumbnail_image)
                    file = open(thumbnail_name,'rb')
                    file_binary = file.read()
                    license_plate_recognized_image = file_binary.encode("base64")
                    file.close()

                    req = requests.post(self.dump_api+"/vehicles/plates", json={
                        'rule_type':'lpr-event',
                        'camera_id': self.cam_id,
                        "timestamp": str(datetime.now(pytz.utc).isoformat()),
                        'data':{
                        'plate': dump_db_item['plate'],
                        'thumbnail': license_plate_recognized_image}
                        })
                    print("result return : ",req.text)
                    os.remove(thumbnail_name)
                except requests.exceptions.Timeout:
                    logger.error("Request Timeout ", exc_info=True)
                except requests.exceptions.HTTPError:
                    logger.error("HTTP Error ", exc_info=True)
                except requests.exceptions.RequestException:  # This is the correct syntax
                    logger.error("Request Exception", exc_info=True)
                except:
                    print(sys.exc_info())
                del self.dump_db[i]

    def __init__(self, darkpy_code='nfdarkpy'):

        self.db_address = data_cfg['db_address']
        self.db_username = data_cfg['db_username']
        self.db_password = data_cfg['db_password']
        self.db_port = data_cfg['db_port']
        self.db_name = data_cfg['db_name']
        self.dump_api = dump_api

        self.color_quantization = ColorQuantization()
        self.rule_detect = None
        self.img = None
        self. sampling_counter=1


        PostProcessing.__init__(self,
                                _data_vehicle,
                                _cfg_vehicle,
                                str(_weight_vehicle),
                                str(_th_vehicle),
                                gpu_id_=str(0),
                                silent_mode=1)

        self.nfdarknet = darkpy_code


    def init_analytics(self, video_size):
        '''
        ==================
        Args for input the rule :

            :param labels (list of string) :  ['car', 'truck', 'bus']
            :param maximum_frame_age (int): 50 default
            :param patch_size (tuple): size of a Grid inside a Frame (H,w)->(20,20) in pixel size
            :param video_size  (tuple): size of a frame (H,W)-> (1028,720)
            :param tracker_code (string)= code tracker- 'sort', 'deepsort'
            :param  object_center (string) = code -> 'center' or 'bottom'
                if 'center' is better for vehicle classification
                if 'bottom' is better for person classification
            :param  flag_watermark_logo (bool) = bottom-right nodeflux logo
            :param rescale_bbox_factor (float)= resize bbox detection
                when value (>1.) can help tracker lost.
                in plate detection case, the value rescale_bbox is better to set more than 1. such as 4 or 5
        '''


        self.rule_detect = VehiclePlateDetection(
                                                labels=_labels ,
                                                video_size=video_size,
                                                patch_size=None,
                                                maximum_frame_age=_max_frame_age,
                                                tracker_code=_tracker_code,
                                                object_center=_object_center, # bottom or center
                                                flag_watermark_logo=True,
                                                rescale_bbox_factor=_rescale_bbox,
                                                flag_two_way=_flag_two_way,
                                                feature_name='VEHICLE-PLATE DETECTION| property of nodeflux',
                                                nfdarknet_code=self.nfdarknet
                                                )
        '''add a roi one by one'''
        for i in range(0,len(_roi)) :
            self.rule_detect.add_roi(_roi[i])

        '''add a roi one by one'''
        for i in range(0,len(_lines)) :
            # line=[_lines[i], _lines[i]]
            # print 'line :', line[i]
            self.rule_detect.add_line(lines=[_lines[i]])

    def pre_processing(self, ori_img):

        # ori_img= resize_by_custom(ori_img,1280,720)
        # ori_img= convert_to_pil(ori_img)
        # if self.color_quantization is None or self.sampling_counter%1000==0:
        #     '''init new color quantization values'''
        #     self.color_quantization = ColorQuantization()
        #
        # ori_img= self.color_quantization.restore(ori_img)
        # ori_img= convert_to_cv(ori_img)
        #
        self.sampling_counter+=1

        return ori_img


    def run_processing(self, img, json_data, callback=None):

        '''
            Procedure yang dipanggil dari PostProccecssing, secara iterate frame by frame
            Args :
                :param img (Array numpy, opencv format): frame
                :param json_data: return dari Predictor berupa Json
                    Example :
                        {u'data': [{u'y2': 399, u'label': u'car', u'x2': 272, u'score': 0.21, u'y1': 345, u'x1': 223},
                         {u'y2': 431, u'label': u'person', u'x2': 531, u'score': 0.18, u'y1': 383, u'x1': 490},
                         {u'y2': 414, u'label': u'car', u'x2': 681, u'score': 0.16, u'y1': 380, u'x1': 616}], u'fps': 22.88}
                :param callback:
                    Function parameter : belum digunakan

            Fungsi lain yang akan dipanggil pada procedure ini yaitu:

            - polygondraw(--) (optional)
            - Init_analytics(--) -->
            - self.mot_tracker.process_json(--)
            - self.rule_detector.process_frame(---)

            - dump data --> setelah panggil self.rule_detector.get_result()
            ---------------------------------------------------------

            '''
        self.img=img
        '''
        activate keybord input,
        push 'c' untuk drawing polygon
        push 'Esc' untuk close drawing
        #-----------------------------
        '''
        key = cv2.waitKey(1) & 0xFF
        if key == ord('c'):  # capturing image

            '''remove  current/old ROI'''
            rois = self.rule_detect.get_roi()
            for id, roi in rois.items():
                self.rule_detect.remove_roi(id)

            '''call a function to draw the ROI'''
            img_roi=self.rule_detect.draw_roi(frame=img)

            rois = self.rule_detect.get_lines  ()
            for id, line in rois.items():
                self.rule_detect.remove_line(id)
            '''call a function to draw the ROI'''
            img_roi=self.rule_detect.draw_line(frame=img_roi)

        if self.rule_detect is None:
            print ('---init---')
            self.init_analytics((img.shape[0], img.shape[1]))

        self.rule_detect.add_data(data=json_data)
        self.rule_detect.add_render_frame(frame=img)
        self.rule_detect.process_data()
        # self.rule_detect.result_data()

        # Dumping here !!
        '''
        example : data dumping
        [{'attribute': {'image_plate': array([[]], dtype=uint8),
        'plate_no': 'B1057WOK', 'image_vehicle': array([[]], dtype=uint8),
        'class': 'VEHICLE',
        'id_area': ''}}]
        '''

        dumped_list = self.rule_detect.result_data()
#       print("here is the result : ",dumped_list)
        if dumped_list is not None:
#            print("result existed")

            for item in dumped_list:
                temp_db = dict()
#                print(temp_db['plate'])
                fname = "test.jpg"
                temp_db['thumbnail'] = temp_dir_path+'/'+fname
                temp_db['plate'] = item['attribute']['plate_no']
                temp_db['plate_image'] = item['attribute']['image_plate']
                temp_db['vehicle_type'] = item['attribute']['class']
                temp_db['image_vehicle'] = item['attribute']['image_vehicle']
                print(temp_db['plate'])
                self.dump_db.append(temp_db)
                1
        thr_dump_data = Thread(target=self.dump_data, args=())
        thr_dump_data.start()
        # print self.rule_detect.result_data()

    def get_visualization(self):
        # return : self.img
        return  self.rule_detect.result_render_frame()


if __name__ == '__main__':
    ''' worker initialization for plate and characters '''
    parent_path_model_plate = os.path.expanduser('~') + '/nodeflux-zoo/yolo-v2-old/plate_new/'
    # parent_path_model_plate = 'plate/'
    __data_plate = parent_path_model_plate + 'license_plate.data'
    __cfg_plate = parent_path_model_plate + 'inf_tiny-license_plate.cfg'
    __weight_plate = parent_path_model_plate + 'plate_best.weights'
    __namelist_plate = parent_path_model_plate + 'license_plate.names'
    __gpu_id_plate = 0
    __th_plate = 0.24

    parent_path_model_chars = os.path.expanduser('~') + '/nodeflux-zoo/yolo-v2-old/char_new/'
    # parent_path_model_chars = 'chars/'
    __data_char = parent_path_model_chars + 'char.data'
    __cfg_char = parent_path_model_chars + 'yolo.cfg'
    __weight_char = parent_path_model_chars + 'chars_best.weights'
    __namelist_char = parent_path_model_chars + 'char.name'
    __gpu_id_char = 0
    __th_char = 0.2

    parent_path_model_vehicle = os.path.expanduser('~') + '/nodeflux-zoo/yolo-v2-old/vehicle_detection/'
    # parent_path_model_vehicle = 'vehicle/'
    _data_vehicle = parent_path_model_vehicle + 'vehicle_detection.data'
    _cfg_vehicle = parent_path_model_vehicle + 'vehicle_detection_yolo_v2.cfg'
    _weight_vehicle = parent_path_model_vehicle + 'vehicle_detection_yolo_best_v4.weights'
    _namelist_vehicle = parent_path_model_vehicle + 'vehicle_detection.names'
    _gpu_id_vehicle = 0
    _th_vehicle = 0.23

    # _endpoint = 'rtsp://nodeflux:Nodeflux123@10.212.45.164/src/Mediainput/stream_1'
    # _roi = [[(16, 161), (943, 104), (1522, 1053), (245, 1064), (16, 161)]]
    #_endpoint='/media/nodeflux/DATA/video/video_car/PC070009.mp4'
    _roi=[[(73, 69), (1086, 46), (1217, 695), (26, 705), (73, 69)]]

    _lines=[[ (40, 265),(1148, 267)]]
    _labels = load_lazyText(parent_path_model_vehicle + 'vehicle_detection.names')
    _max_frame_age = 15.
    _rescale_bbox = [3,1]
    _tracker_code = 'sort'
    _object_center='center' # 'center' or 'bottom'
    _flag_two_way=False

    print("initialize rule license plate recognition version : ",__version__)

    db_address = data_cfg['db_address']
    db_username = data_cfg['db_username']
    db_password = data_cfg['db_password']
    db_port = data_cfg['db_port']
    db_name = data_cfg['db_name']
    dump_api = data_cfg['api_url']
    silent_mode = data_cfg['silent_mode']

    trx_id = sys.argv[1]
    fmmap_in = sys.argv[2]
    fmmap_out = sys.argv[3]
    fmmap_size = sys.argv[4]

    try:
        conn = psycopg2.connect("dbname= " + db_name+" user= "+db_username +
                                "  host= " + db_address+"  password= "+db_password)
    except:
        logger.error(sys.exc_info())

    cur = conn.cursor()

    cur.execute(
        "SELECT additional_parameter, camera_id FROM transaction_camera_rule_vas_collection where id = "+trx_id)
    r = cur.fetchall()

    camera_id = int(r[0][1])
    gpu_id = int(sys.argv[5])
    fps_inferencing = r[0][0]['fps_inferencing']
    darknet_thresh = r[0][0]['darknet_thresh']
    max_age = r[0][0]['max_age']
    confidence = r[0][0]['confidence']
    non_maxima_suppression = r[0][0]['nms']
    min_sample_data = r[0][0]['min_sample']
    max_sample_data = r[0][0]['max_sample']
    distance_ratio = r[0][0]['distance_ratio']
    code = r[0][0]['code']
    roi = r[0][0]['roi']
        
    _roi = roi

    # '''
    # INFO :
    #====================
    # code for inference type:
    # darkpy    : old inference yolo
    # nfdarkpy  : new inference yolo
    # predictor : need mmap
    # '''
    _nfdarkpy_code = 'darkpy'

    # ''' worker initialization for vehicle'''
    plate_detector = LprDetectionWorker(__data_plate, __cfg_plate, __weight_plate, __namelist_plate, __th_char,
                                        __data_char, __cfg_char, __weight_char, __namelist_char, __th_char,
                                        max_overlap=0.23, min_data_char=2, max_data_char=6,
                                        flag_interrupt_dump=False,
                                        gpu_id_plate=__gpu_id_plate,
                                        gpu_id_char=__gpu_id_char,
                                        single_image=False,
                                        code=_nfdarkpy_code)
    plate_detector.run()

    test = VehicleClassificationPlateMain(darkpy_code='predictor')
    test.run()
